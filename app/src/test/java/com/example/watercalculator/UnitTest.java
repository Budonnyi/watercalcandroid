package com.example.watercalculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class UnitTest {
    @Test
    public void calculation_for_male() {
        Tab1Fragment tab1Fragment = new Tab1Fragment();
        double val = tab1Fragment.calcForMale(50, 4);
        assertEquals(4.10332, val, 0.0001);
    }

    @Test
    public void calculation_for_female() {
        Tab1Fragment tab1Fragment = new Tab1Fragment();
        double val = tab1Fragment.calcForFemale(50, 4);
        assertEquals(3.9, val, 0.0001);
    }

    @Test
    public void string_builder() {
        Tab2Fragment tab2Fragment = new Tab2Fragment();
        String val = tab2Fragment.buildStr(1, 2, 50, 2, "Male", "10.10.2010");
        assertEquals("1) 2.0 - 50.0 - 2.0 - Male - 10.10.2010\n", val);
    }
}