package com.example.watercalculator;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

public class PopTab4 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pop_window_4);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;


        getWindow().setLayout((int)(width * .85), (int)(height * .85));
    }
}
