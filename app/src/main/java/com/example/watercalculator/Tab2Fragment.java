package com.example.watercalculator;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tab2Fragment extends Fragment {

    TextView tvHistory;
    Button btnHistory;
    CheckBox cbReminder;
    private double water = 0;

    // Start the service
    public void startService(View view) {
        Intent intent = new Intent(getActivity(), NotificationService.class);
        intent.putExtra("waterAmount", water);
        getActivity().startService(intent);
    }
    // Stop the service
    public void stopService(View view) {
        getActivity().stopService(new Intent(getActivity(), NotificationService.class));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab2_fragment,container,false);

        tvHistory = (TextView) view.findViewById(R.id.tvHistory);
        btnHistory = (Button) view.findViewById(R.id.btnHistory);
        cbReminder = (CheckBox) view.findViewById(R.id.cbReminder);

        tvHistory.setMovementMethod(new ScrollingMovementMethod());

        btnHistory.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getAllHistory();

                        if (cbReminder.isChecked()) {
                            stopService(getView());
                            startService(getView());
                        }
                    }
                });

        cbReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked) {
                    startService(getView());
                } else {
                    stopService(getView());
                }

            }
        });

        return view;
    }

    public String buildStr(int id, double water, double weight, double activity, String gender, String data){
        return id + ") " + water + " - " + weight + " - " + activity + " - "  + gender + " - " + data + "\n";
    }

    void getAllHistory() {

        class GetRecords extends AsyncTask<Void, Void, Void>{
            String data_str = "ID - Water - Weight - Activity - Gender - Date\n";

            @Override
            protected Void doInBackground(Void... voids) {
                List<Record> records = new ArrayList<Record>();
                records = DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .recordDao()
                        .getAll();
                for (Record i : records) {
                    water = i.getWater();
                    data_str = data_str + buildStr(i.getId(), i.getWater(),i.getWeight(),i.getActivity(), i.getGender(), i.getDate());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                tvHistory.setText(data_str);
            }
        }
        GetRecords st = new GetRecords();
        st.execute();
    }

//    double getWaterFromLastRecord() {
//
//        class GetLastWater extends AsyncTask<Void, Void, Void>{
//
//            @Override
//            protected Void doInBackground(Void... voids) {
//                List<Record> records = new ArrayList<Record>();
//                records = DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
//                        .recordDao()
//                        .getAll();
//                water = records.get(-1).getWater();
//                return null;
//            }
//        }
//        GetLastWater st = new GetLastWater();
//        st.execute();
//
//        return water;

//        String history = tvHistory.getText().toString();
//        double water;
//        if (history != "") {
//            String[] history_array = history.split("\n");
//            String[] record_list = history_array[-2].split(" ");
////            water = Double.parseDouble(record_list[2]);
//            water = 1;
//        } else {
//            water = 0;
//        }

//        class getWaterFromLastRecord extends AsyncTask<Void, Void, Void>{
//            double water_data = 0;
//
//            @Override
//            protected Void doInBackground(Void... voids) {
////                List<Record> records = new ArrayList<Record>();
////                records = DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
////                        .recordDao()
////                        .getAll();
////                Record lastRecord = records.get(-1);
////                water_data = lastRecord.getWater();
//                Tab1Fragment.waterAmount = 2;
//                return null;
//            }
//
//        }
//        getWaterFromLastRecord st = new getWaterFromLastRecord();
//        st.execute();
//    }
}
