package com.example.watercalculator;

        import android.arch.persistence.room.Dao;
        import android.arch.persistence.room.Delete;
        import android.arch.persistence.room.Insert;
        import android.arch.persistence.room.Query;
        import android.arch.persistence.room.Update;

        import java.util.List;

@Dao
public interface RecordDao {

    @Query("SELECT * FROM Record")
    List<Record> getAll();

    @Insert
    void insert(Record task);

    @Delete
    void delete(Record task);

    @Update
    void update(Record task);

}
