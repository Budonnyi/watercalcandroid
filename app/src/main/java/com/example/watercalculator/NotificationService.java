package com.example.watercalculator;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;
import android.os.Handler;

import java.text.DateFormat;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.function.DoubleToIntFunction;

public class NotificationService extends Service{
    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;

    @Override
    public IBinder onBind(Intent intent) {
        return  null;
    }
    @Override
    public void onCreate() {
        Toast.makeText(this, "Reminder enabled", Toast.LENGTH_LONG).show();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final double waterAmount = intent.getDoubleExtra("waterAmount", 0.0);
        Toast.makeText(this, "Service Started " + waterAmount, Toast.LENGTH_LONG).show();

        double waterCupsDouble = waterAmount/0.2;
        double periodDouble = 14*60*100/waterCupsDouble;
        int period = (int) periodDouble;

        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                try {
                    String string1 = "08:00:00";
                    Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(time1);

                    String string2 = "22:00:00";
                    Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTime(time2);
                    calendar2.add(Calendar.DATE, 1);

                    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                    Date date = new Date();
                    String currentTime = dateFormat.format(date);
                    Date d = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
                    Calendar calendar3 = Calendar.getInstance();
                    calendar3.setTime(d);
                    calendar3.add(Calendar.DATE, 1);

                    Date x = calendar3.getTime();
                    if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                        NotificationManager notif=(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        Notification notify=new Notification.Builder
                                (getApplicationContext()).setContentTitle("Drink water!").
                                setContentText("Your daily rate is " + waterAmount).
                                setContentTitle("Drink water!").
                                setSmallIcon(R.mipmap.ic_launcher_round).
                                build();

                        notify.flags |= Notification.FLAG_AUTO_CANCEL;
                        notif.notify(0, notify);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                handler.postDelayed(runnable, 5000);
            }
        };

        handler.postDelayed(runnable, 5000);

        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
        Toast.makeText(this, "Reminder disabled", Toast.LENGTH_LONG).show();
    }
}
