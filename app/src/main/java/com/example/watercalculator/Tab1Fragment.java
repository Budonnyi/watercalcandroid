package com.example.watercalculator;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

public class Tab1Fragment extends Fragment {

    EditText tvWeight;
    EditText tvPhysicalActivity;
    Button btnCalculate;
    TextView tvCalculatedVolume;
    RadioGroup rbGender;
    RadioButton radioGenderButton;

    public double calcForMale(double weight, double phys_activity){
        return weight * 0.0314 + phys_activity * 0.63333;
    }

    public double calcForFemale(double weight, double phys_activity){
        return weight * 0.03 + phys_activity * 0.6;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab1_fragment,container,false);

        tvWeight = (EditText) view.findViewById(R.id.tvWeight);
        tvPhysicalActivity = (EditText) view.findViewById(R.id.tvPhysicalActivity);
        btnCalculate = (Button) view.findViewById(R.id.btnCalculate);
        tvCalculatedVolume = (TextView) view.findViewById(R.id.tvCalculatedVolume);
        rbGender = (RadioGroup) view.findViewById(R.id.rbGender);

        btnCalculate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get selected radio button from radioGroup
                        int selectedId = rbGender.getCheckedRadioButtonId();

                        // find the radiobutton by returned id
                        radioGenderButton = (RadioButton) getView().findViewById(selectedId);

                        double weight = Double.parseDouble(tvWeight.getText().toString().trim());
                        double phys_activity = Double.parseDouble(tvPhysicalActivity.getText().toString().trim());
                        double val;
                        String gender = radioGenderButton.getText().toString();

                        if (gender.equals("Male")) {
                            val = calcForMale(weight, phys_activity);
                        } else {
                            val = calcForFemale(weight, phys_activity);
                        }

                        String string_val = String.format("%.2f", val);

                        // Save to DB
                        saveRecord(weight, phys_activity, Double.parseDouble(string_val), gender);

                        // Write number to tvCalculatedVolume
                        tvCalculatedVolume.setText(string_val);
                    }
                });

        return view;
    }


    private void saveRecord(final double weight, final double phys_activity, final double water, final String gender) {

        class SaveRecord extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                //creating a task
                Record record = new Record();
                record.setDate(dateFormat.format(date));
                record.setWeight(weight);
                record.setActivity(phys_activity);
                record.setGender(gender);
                record.setWater(water);

                //adding to database
                DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .recordDao()
                        .insert(record);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
                Toast.makeText(getActivity().getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveRecord st = new SaveRecord();
        st.execute();
    }

}
