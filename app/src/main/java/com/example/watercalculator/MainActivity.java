package com.example.watercalculator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private com.example.watercalculator.SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: Starting.");

        mSectionsPageAdapter = new com.example.watercalculator.SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        com.example.watercalculator.SectionsPageAdapter adapter = new com.example.watercalculator.SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new com.example.watercalculator.Tab1Fragment(), "CALCULATE");
        adapter.addFragment(new com.example.watercalculator.Tab2Fragment(), "HISTORY");
        adapter.addFragment(new com.example.watercalculator.Tab3Fragment(), "Accelerometer");
        adapter.addFragment(new com.example.watercalculator.Tab4Fragment(), "GPS");

        viewPager.setAdapter(adapter);
    }

}
